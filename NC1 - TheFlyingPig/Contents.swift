import UIKit
import PlaygroundSupport
import Foundation
import AVFoundation

//to play music
class MusicPlayer {
    static let shared = MusicPlayer()
    var audioPlayer: AVAudioPlayer?
    var player: AVAudioPlayer?
    
    func startBackgroundMusic(backgroundMusicFileName: String) {
        if let bundle = Bundle.main.path(forResource: backgroundMusicFileName, ofType: "mp3") {
            let backgroundMusic = NSURL(fileURLWithPath: bundle)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf:backgroundMusic as URL)
                guard let audioPlayer = audioPlayer else { return }
                audioPlayer.numberOfLoops = -1
                audioPlayer.prepareToPlay()
                audioPlayer.volume = 0.5
                audioPlayer.play()
            } catch {
                print(error)
            }
        }
    }
    
    func stopBackgroundMusic() {
           guard let audioPlayer = audioPlayer else { return }
           audioPlayer.stop()
       }
    
    func playOink() {
        guard let url = Bundle.main.url(forResource: "oink", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

           
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            guard let player = player else { return }
        
            player.play()
            player.volume = 10
            
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func playPoof() {
        guard let url = Bundle.main.url(forResource: "poof", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

           
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            guard let player = player else { return }
        
            player.play()
            player.volume = 10
            
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
}





//PAGE 0
class ViewController0: UIViewController {
    
        var pigView: UIImageView!
        var backgroundView: UIImageView!
        var textView: UIImageView!
        var textView2: UIImageView!
        var backgView: UIImageView!
        var nPage: UILabel!
        var buttonN: UIButton!
        var flag: Int! = 0
        var OnceView: UIImageView!
        var FlyingView: UIImageView!
   

   
    
    
   

    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        
        
        backgView = UIImageView()
        backgView.image = UIImage(named: "sfondo.jpg")
        backgView.frame = CGRect(x: -120, y: 0, width: 700, height: 750)
        
        OnceView = UIImageView()
        OnceView.image = UIImage(named: "OnceuponTime.png")
        OnceView.frame = CGRect(x: 30, y: -150, width: 380, height: 380)
               
        FlyingView = UIImageView()
        FlyingView.image = UIImage(named: "flyingPig.png")
        FlyingView.frame = CGRect(x: -85, y: -175, width: 550, height: 500)
        FlyingView.isHidden = true

        nPage=UILabel()
        nPage.text="1"
        nPage.font = .boldSystemFont(ofSize: 10)
        nPage.textColor = .black
        nPage.frame = CGRect(x: 180, y: 640, width: 10, height: 10)
        
        backgroundView = UIImageView()
        backgroundView.image = UIImage(named: "background1.jpg")
        backgroundView.frame = CGRect(x: 0, y: 0, width: 480, height: 680)
        backgroundView.isHidden = true
        
        pigView = UIImageView()
        pigView.image = UIImage(named: "pig.png")
        pigView.frame = CGRect(x: 140, y: 390, width: 180, height: 180)
        let tapP = UITapGestureRecognizer(target: self, action: #selector(pigTap))
        pigView.isUserInteractionEnabled = true
        pigView.addGestureRecognizer(tapP)
        pigView.isHidden = true
        
        textView = UIImageView()
        textView.image=UIImage(named: "speech_hello.png")
        textView.frame=CGRect(x: 10, y: 270, width: 230, height: 200)
        textView.isHidden=true
        
        textView2 = UIImageView()
        textView2.image=UIImage(named: "speech_lemonades.png")
        textView2.frame=CGRect(x: 10, y: 270, width: 230, height: 200)
        textView2.isHidden=true
        
        //dopo 6 secondi...
        DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
           // self.textLabel.isHidden = true
            self.FlyingView.isHidden = false
            self.pigView.isHidden = false
            self.backgroundView.isHidden = false
            self.OnceView.isHidden=true
            // MusicPlayer.shared.stopBackgroundMusic()
             MusicPlayer.shared.startBackgroundMusic(backgroundMusicFileName: "trollSong")
        }
        
        view.addSubview(backgView)
        view.addSubview(backgroundView)
        view.addSubview(OnceView)
        view.addSubview(FlyingView)
        view.addSubview(textView)
        view.addSubview(textView2)
        view.addSubview(pigView)
        view.addSubview(nPage)
        
        self.view = view
        
        MusicPlayer.shared.startBackgroundMusic(backgroundMusicFileName: "music")
        
    }
    
    
    
   
    
    @objc func pigTap()
      {
        MusicPlayer.shared.playOink()
           
        if(flag==0){
             flag=1
           // print("flag=1")
              textView.isHidden=false
        }
        else if(flag==1){
            flag=2
          //  print("flag=2")
              textView2.isHidden=false
              textView.isHidden=true
        }
        else{
            textView2.isHidden=true
            textView.isHidden=true
            flag=0
            let viewController1 = ViewController1()
            PlaygroundPage.current.liveView = viewController1
            
        }
           
      }
    
    
    func movePig() {

        UIView.animate(withDuration: 0.5, delay: 1,
          options: [.repeat, .autoreverse, .allowUserInteraction],
          animations: {
            self.pigView.center.y += self.view.bounds.width-350
          },
          completion: nil
        )
    }
    
    func moveLabel(){
        UIView.animate(withDuration: 3.5, delay: 0,
          options: [.curveEaseOut],
          animations: {
              self.OnceView.center.y += self.view.bounds.width-150
            
          },
          completion: nil
        )
    }
    
    
   override func viewDidAppear(_ animated: Bool) {
        moveLabel()
        movePig()
    }
}









//CHAPTER 1
class ViewController1: UIViewController {
    
    var backgroundView: UIImageView!
    var pigView: UIImageView!
    var pigViewB: UIImageView!
    var dragonView: UIImageView!
    var dragonViewB: UIImageView!
    var rideView: UIImageView!
    var wooView: UIImageView!
    var thinkView: UIImageView!
    var buttonP: UIButton!
    var buttonN: UIButton!
    var nPage: UILabel!
    let impact = UIImpactFeedbackGenerator()

    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .systemBlue
        
        
        backgroundView = UIImageView()
        backgroundView.image = UIImage(named: "sky.jpg")
        backgroundView.frame = CGRect(x: -200, y: 0, width: 600, height: 680)

        
        
        nPage=UILabel()
        nPage.text="2"
        nPage.font = .boldSystemFont(ofSize: 10)
        nPage.textColor = .black
        nPage.frame = CGRect(x: 180, y: 640, width: 10, height: 10)

        
        rideView = UIImageView()
        rideView.image = UIImage(named: "speech_ride.png")
        rideView.frame=CGRect(x: 5, y: 20, width: 180, height: 200)
                    
        
        
        
        wooView = UIImageView()
        wooView.image = UIImage(named: "speech_woo.png")
        wooView.frame=CGRect(x: 5, y: 100, width: 150, height: 250)

        thinkView = UIImageView()
        thinkView.image = UIImage(named: "thought_dragon.png")
        thinkView.frame=CGRect(x: 80, y: -100, width: 330, height: 450)
        
        
        
        
        pigView = UIImageView()
        pigView.image = UIImage(named: "pig.png")
        pigView.frame = CGRect(x: 103, y: 245, width: 100, height: 100)
        pigView.transform = pigView.transform.rotated(by: 5.6)
        
        
        pigViewB = UIImageView()
        pigViewB.image = UIImage(named: "pig.png")
        pigViewB.frame = CGRect(x: 125, y: 140, width: 100, height: 100)
        let tapP = UITapGestureRecognizer(target: self, action: #selector(movePigB))
        pigViewB.isUserInteractionEnabled = true
        pigViewB.addGestureRecognizer(tapP)

        dragonView = UIImageView()
        dragonView.image = UIImage(named: "dragon.png")
        dragonView.frame = CGRect(x: 15, y: 200, width: 350, height: 250)
        
        dragonViewB = UIImageView()
        dragonViewB.image = UIImage(named: "dragon.png")
        dragonViewB.frame = CGRect(x: 10, y: 370, width: 350, height: 250)
        
        
      /* pigViewB.isUserInteractionEnabled = true
        let dragInteraction = UIDragInteraction(delegate: self)
                 pigViewB.addInteraction(dragInteraction)
        
        */
        

        
        
        pigViewB.image = UIImage(named: "pig.png")
        pigViewB.contentMode = .scaleAspectFit
        pigViewB.isUserInteractionEnabled = true
      
        
      
        
        
        view.addSubview(backgroundView)
        
       
        view.addSubview(rideView)
       
        
        pigView.isHidden=true
        dragonView.isHidden=true
        
        view.addSubview(dragonViewB)
        view.addSubview(pigViewB)
        view.addSubview(nPage)
        view.addSubview(wooView)
        view.addSubview(thinkView)
        view.addSubview(dragonView)
        view.addSubview(pigView)
        wooView.isHidden=true
        thinkView.isHidden=true
       
    
        self.view = view
    }
    
    
    func  MoveSky(){
        UIView.animate(withDuration: 5.5, delay: 1,
                options: [.repeat, .autoreverse],
                animations: {
                  self.backgroundView.center.x += self.view.bounds.width-200
                },
                completion: nil
              )
    }
    
    func movePig() {

          UIView.animate(withDuration: 1.6, delay: 1,
            options: [.repeat, .autoreverse],
            animations: {
              self.pigView.center.y += self.view.bounds.width-300
            }
            //completion: nil
          )
         }
    
    
    func moveDragon() {
       UIView.animate(withDuration: 1.6, delay: 1,
         options: [.repeat, .autoreverse],
         animations: {
           self.dragonView.center.y += self.view.bounds.width-300
         },
         completion: nil
       )
       
    }
    
    
    func movePigB2() {

             UIView.animate(withDuration: 0.5, delay: 1,
                      options: [.repeat, .autoreverse, .allowUserInteraction],
                      animations: {
                        self.pigViewB.center.y += self.view.bounds.width-350
                      },
                      completion: nil
                    )
            }
    
    @objc func movePigB() {
        
        MusicPlayer.shared.playOink()

        self.rideView.isHidden=true
            UIView.animate(withDuration: 1.5, delay: 0,
           options: [.curveEaseInOut , .allowUserInteraction],
                animations: {
                    self.pigViewB.center = CGPoint(x: self.view.frame.width - 208, y: self.view.frame.width + 85)
                    self.pigViewB.transform = CGAffineTransform(rotationAngle: 50.0)
                    
                }
            
            )
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.4) {
                self.pigViewB.isHidden=true
                self.dragonViewB.isHidden=true
                self.pigView.isHidden=false
                self.dragonView.isHidden=false
                self.thinkView.isHidden=false
                self.wooView.isHidden=false
                self.movePig()
                self.moveDragon()
                          }
        
        //dopo 5 secondi...
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.5) {
               
            
               
                let viewController = ViewController2()
                PlaygroundPage.current.liveView = viewController
                MusicPlayer.shared.stopBackgroundMusic()
                }
           
        }

    
       
    
    /*
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
       
        switch sender.state {
           
        case .possible:
            break
        case .began:
            print("sto muovendo")
        case .changed:
            //print("sto cambiando")
            self.view.bringSubviewToFront(pigViewB)
            let translation = sender.translation(in: self.view)
            pigViewB.center = CGPoint(x: pigViewB.center.x + translation.x, y: pigViewB.center.y + translation.y)
            sender.setTranslation(CGPoint.zero, in: self.view)
        case .ended:
            //print("ho finito")
            let maxY = dragonViewB.frame.origin.y+40
             let maxY2 = pigViewB.frame.origin.y
            if(maxY2>=maxY){
                print("sto volando")
                pigView.isHidden=false
                dragonView.isHidden=false
                pigViewB.isHidden=true
                               dragonViewB.isHidden=true
                movePig()
                moveDragon()
                
                //dopo 5 secondi...
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {

                   let viewController = ViewController2()
                   PlaygroundPage.current.liveView = viewController
                   MusicPlayer.shared.stopBackgroundMusic()
                }
            }
            else{
                pigComeBack()
            }
        case .cancelled:
            print("cancellato")
        case .failed:
            break
        @unknown default:
            print("default")
        }
        
    }
    
    
    func setDragDelegates(){
        
        var panGesture = UIPanGestureRecognizer()
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedView(_:)))
        
        pigViewB.isUserInteractionEnabled = true
        pigViewB.addGestureRecognizer(panGesture)
    }
     
     
     func pigComeBack() {
               UIView.animate(withDuration: 1.5, delay: 1,
                 animations: {
                   self.pigViewB.center = CGPoint(x:self.view.frame.size.width/2, y: 150)
                 }
               )
            }
    */
    
    
    
    
    
   

    
  
    
   
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // setDragDelegates()
    }
    
   override func viewDidAppear(_ animated: Bool) {
       MoveSky()
       MusicPlayer.shared.startBackgroundMusic(backgroundMusicFileName: "wind")
           movePigB2()
    }
}





//PAGE 2
class ViewController2: UIViewController {
    
  
        var pigView: UIImageView!
        var grannyView: UIImageView!
        var lemonadeView: UIImageView!
        var drinkView: UIImageView!
        var wtfView: UIImageView!
        var lemonadeView2: UIImageView!
        var womanView: UIImageView!
        var poofView: UIImageView!
        var volta: Int! = 0

  
  override func loadView()
  {
            let view = UIView()
            view.backgroundColor = .black

            let imageKitchen = UIImage(named:"kitchen.png")
            let viewImageKitchen = UIImageView(image : imageKitchen)
            viewImageKitchen.frame = CGRect(x: -50, y: 0, width: 450, height: 670)
            view.addSubview(viewImageKitchen)
    
    
            drinkView = UIImageView()
            drinkView.image=UIImage(named: "speech_drink.png")
            drinkView.frame=CGRect(x: 100, y: 270, width: 210, height: 170)
            view.addSubview(drinkView)

            wtfView = UIImageView()
            wtfView.image=UIImage(named: "speech_wtf.png")
            wtfView.frame=CGRect(x: 80, y: 280, width: 210, height: 170)
            view.addSubview(wtfView)
            wtfView.isHidden=true

            lemonadeView = UIImageView()
            lemonadeView.image = UIImage(named:"glass_of_lemonade.png")
            lemonadeView.frame = CGRect(x: 125, y: 445, width: 80, height: 110)
            let tapP = UITapGestureRecognizer(target: self, action: #selector(lemonadeTap))
            lemonadeView.isUserInteractionEnabled = true
            lemonadeView.addGestureRecognizer(tapP)

            view.addSubview(lemonadeView)

            lemonadeView2 = UIImageView()
            lemonadeView2.image = UIImage(named:"glass.png")
            lemonadeView2.frame = CGRect(x: 125, y: 445, width: 80, height: 110)
            lemonadeView2.isHidden=true
            view.addSubview(lemonadeView2)

            pigView = UIImageView()
            pigView.image = UIImage(named: "pig_reverse.png")
            pigView.frame = CGRect(x: 10, y: 365, width: 170, height: 180)
            pigView.isUserInteractionEnabled = true
            pigView.addGestureRecognizer(tapP)
            view.addSubview(pigView)

            grannyView = UIImageView()
            grannyView.image = UIImage(named:"grandmom.png")
            grannyView.frame = CGRect(x: 220, y: 365, width: 120, height: 180)
            view.addSubview(grannyView)

            womanView = UIImageView()
            womanView.image = UIImage(named:"hot_girl.png")
            womanView.frame = CGRect(x: 180, y: 180, width: 280, height: 400)
            view.addSubview(womanView)
            womanView.isHidden=true
       
            poofView = UIImageView()
            poofView.image = UIImage(named:"POOF.png")
            poofView.frame = CGRect(x: 190, y: 305, width: 230, height: 280)
            view.addSubview(poofView)
            poofView.isHidden=true

            self.view = view
}
    
   func movePig()
   {

       UIView.animate(withDuration: 0.5, delay: 1,
         options: [.repeat, .autoreverse, .allowUserInteraction],
         animations:
         {
           self.pigView.center.y += self.view.bounds.width-350
           self.lemonadeView.center.y += self.view.bounds.width-350
              self.lemonadeView2.center.y += self.view.bounds.width-350

         },
         completion: nil
       )
   }
    
   @objc func lemonadeTap()
   {
    
        MusicPlayer.shared.playOink()
        drinkView.isHidden=true
        self.lemonadeView2.isHidden=false
        self.lemonadeView.isHidden=true
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.65){
            MusicPlayer.shared.playPoof()
            self.grannyView.isHidden = true
            self.poofView.isHidden=false

        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.65) {
            self.womanView.isHidden=false
            self.poofView.isHidden=true

        }
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.50) {
            self.wtfView.isHidden=false

        }
            
        DispatchQueue.main.asyncAfter(deadline: .now() + 7.0) {
            let viewController = ViewController3()
            PlaygroundPage.current.liveView = viewController
        }
        
    
        lemonadeView.isUserInteractionEnabled = false
        pigView.isUserInteractionEnabled = false
   }
   override func viewDidAppear(_ animated: Bool)
   {
        movePig()
   }
    

}




//PAGE 3
class ViewController3: UIViewController {
    
   var backgroundView: UIImageView!
       
        var richPigView: UIImageView!
        var bankView: UIImageView!
        var thanksView: UIImageView!
        var banquetView: UIImageView!
        var money0View: UIImageView!
        var money1View: UIImageView!
        var money2View: UIImageView!
        var money3View: UIImageView!
        var money4View: UIImageView!
        var money5View: UIImageView!
        var money6View: UIImageView!
        var money7View: UIImageView!
        var money8View: UIImageView!
        var elderly0View: UIImageView!
        var elderly1View: UIImageView!
        var elderly2View: UIImageView!

        
    //Funzione per far muovere il maiale
        func movePig() {

        UIView.animate(withDuration: 0.5, delay: 0,
            options: [.repeat, .autoreverse, .allowUserInteraction],
            animations: {
            self.richPigView.center.y += self.view.bounds.width-350
            },
            completion: nil
        )
        }
    

        
        
        override func loadView() {
            let view = UIView()
            
    //Imposto lo sfondo
            backgroundView = UIImageView()
            backgroundView.image = UIImage(named: "scenery.jpg")
            backgroundView.frame = CGRect(x: -105, y: 0, width: 480, height: 680)
            view.addSubview(backgroundView)

            bankView = UIImageView()
            bankView.image=UIImage(named: "speech_bank.png")
            bankView.frame=CGRect(x: 0, y: 290, width: 210, height: 170)

            thanksView = UIImageView()
            thanksView.image=UIImage(named: "speech_thanks.png")
            thanksView.frame=CGRect(x: 0, y: 290, width: 210, height: 210)
             
            
    //Soldi Destra
            money0View = UIImageView()
            money0View.image = UIImage(named: "bag_of_money.png")
            money0View.frame = CGRect(x: 100, y: 445, width: 70, height: 70)
            view.addSubview(money0View)
    //Maiale
            richPigView = UIImageView()
            richPigView.image = UIImage(named: "rich_pig.png")
            richPigView.frame = CGRect(x: 130, y: 390, width: 150, height: 150)
            let tapP = UITapGestureRecognizer(target: self, action: #selector(pigTap))
            richPigView.isUserInteractionEnabled = true
            richPigView.addGestureRecognizer(tapP)
            
            
          
    //Soldi Sinistra
            money1View = UIImageView()
            money1View.image = UIImage(named: "bag_of_money.png")
            money1View.frame = CGRect(x: 35, y: 470, width: 70, height: 70)
            view.addSubview(money1View)
            
    //Soldi Centro
            money2View = UIImageView()
            money2View.image = UIImage(named: "bag_of_money.png")
            money2View.frame = CGRect(x: 60, y: 455, width: 70, height: 70)
            view.addSubview(money2View)

            
    //Soldi a destra del maiale 1
            money4View = UIImageView()
            money4View.image = UIImage(named: "bag_of_money.png")
            money4View.frame = CGRect(x: 230, y: 450, width: 70, height: 70)
            view.addSubview(money4View)
    //Soldi a destra del maiale 2
            money5View = UIImageView()
            money5View.image = UIImage(named: "bag_of_money.png")
            money5View.frame = CGRect(x: 275, y: 450, width: 70, height: 70)
            view.addSubview(money5View)
          
    //Vecchia 1
            elderly0View = UIImageView()
            elderly0View.image = UIImage(named: "elderly_1.png")
            elderly0View.frame = CGRect(x: 140, y: 120, width: 300, height: 390)
            view.addSubview(elderly0View)
    //Vecchia 2
            elderly1View = UIImageView()
            elderly1View.image = UIImage(named: "elderly_2.png")
            elderly1View.frame = CGRect(x: 110, y: 190, width: 180, height: 240)
            view.addSubview(elderly1View)
    //Vecchia 3
            elderly2View = UIImageView()
            elderly2View.image = UIImage(named: "elderly_3.png")
            elderly2View.frame = CGRect(x: 40, y: 240, width: 130, height: 130)
            view.addSubview(elderly2View)
    //Banchetto
            banquetView = UIImageView()
            banquetView.image = UIImage(named: "lemonade_banquet.png")
            banquetView.frame = CGRect(x: 0, y: 200, width: 421, height: 512)
               
    //Soldi davanti al banchetto
            money3View = UIImageView()
            money3View.image = UIImage(named: "bag_of_money.png")
            money3View.frame = CGRect(x: 290, y: 580, width: 70, height: 70)
           


            
           
             view.addSubview(richPigView)
             view.addSubview(banquetView)
             view.addSubview(money3View)
             view.addSubview(bankView)
             view.addSubview(thanksView)
            thanksView.isHidden=true
            MusicPlayer.shared.startBackgroundMusic(backgroundMusicFileName: "money")
            
            self.view = view
        }
        
    //Richiamo la funzione per far muovere il maiale
        override func viewDidAppear(_ animated: Bool) {
            movePig()
            }
        
        override func viewDidLoad() {
            super.viewDidLoad()
        }
    
    @objc func pigTap()
    {
           MusicPlayer.shared.playOink()
           thanksView.isHidden=false
           bankView.isHidden=true
    }
          
           
}





let viewController = ViewController0()
PlaygroundPage.current.liveView = viewController
PlaygroundPage.current.needsIndefiniteExecution = true

